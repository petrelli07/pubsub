var subscriptionArray = []

const Axios = require('axios');

exports.createSubscription = async (req,res)=>{
    const {url} = req.body;
    const {topic} = req.params;
    let subscription = {};
    try{
        subscription.topic = topic
        subscription.url = [];
        subscription.url.push(url);
        subscriptionArray.push(subscription);
        res.status(200).json({message:"subscription created"});
    }catch(e){
        console.log(e);
        res.status(500).json({error:e});
    }
}

exports.publishTopic = async (req,res)=>{
    const {topic} = req.params;
    const {message} = req.body;

    let subUrls = subscriptionArray[0].url;

    let sentMessage = {message,topic}
    
    try{
        for(let subUrl of subUrls){
        var axiosRequests = [];
           axiosRequests.push(
            Axios.post(subUrl, sentMessage));
         }
         Axios.all(axiosRequests);
    }catch(e){
        console.log(e);
        res.status(500).json({error:e});
    }
}

exports.event = async(req,res)=>{
    console.log(req.body);
}