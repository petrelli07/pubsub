
const express = require('express');
const app = express();
const router = require('./routes/route');
const cors = require('cors');
const bodyParser = require('body-parser');
const dotenv = require('dotenv').config();
app.disable('etag');
app.use(cors());
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: false
}));


app.use('/', router);

app.listen(process.env.APP_PORT, () => {
    console.log(`Listening on port: ${process.env.APP_PORT} 🌎`);
});

module.exports = app;