const express = require('express');
const router = express.Router();


const PubSubController = require('../controllers/pubSubController');


router.route('/subscribe/:topic').post(PubSubController.createSubscription);
router.route('/publish/:topic').post(PubSubController.publishTopic);
router.route('/event').post(PubSubController.event);


module.exports = router;